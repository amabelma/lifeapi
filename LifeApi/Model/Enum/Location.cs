namespace LifeApi.Model.Enum
{
    public enum Location
    {
        Basement,
        Kitchen,
        LivingRoom,
        Bedroom
    }
}